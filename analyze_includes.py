import argparse
import fileinput

class SourceFile:
  def __init__(self, name):
    self.fileName = name
    self.nrLines = 0
    self.includes = []
    self.isSystemHeader = False

  def __repr__(self):
    return self.fileName

  def lines(self):
    files = set()
    self.addFiles(files)
    nrLines = 0
    for file in files:
      nrLines += file.nrLines
    return nrLines

  def addFiles(self, files):
    files.add(self)
    for file in self.includes:
      if file not in files:
        file.addFiles(files)

  def show(self, depth, stopSysHeader, pre1 = "", pre2 = ""):
    print pre1 + str(self.lines()) + " " + str(self)
    if stopSysHeader and self.isSystemHeader:
      return
    if depth and len(self.includes) > 0:
      for header in self.includes[:-1]:
        header.show(depth - 1, stopSysHeader, pre2 + "+-", pre2 + "| ")
      self.includes[-1].show(depth - 1, stopSysHeader, pre2 + "\-", pre2 + "  ")

def process(line):
  global currentFile
  global sourceFiles
  global rootFile

  if len(line) > 3 and line[0] is '#' and line[1] is ' ' and line[2].isdigit:
    # a line added by the GCC preprocessor
    fields = line.split('"')
    fileName = fields[1]

    # First line is the source file
    if rootFile is None:
      rootFile = SourceFile(fileName)
      sourceFiles.append(rootFile)
      currentFile = rootFile
      return

    flags = fields[2].split()
    if len(flags) < 1:
      return # Ignore these lines (no relevant info)   

    isNewInclude = (int(flags[0]) == 1)
    isReturn = (int(flags[0]) == 2)

    if isNewInclude:
      fileList = filter(lambda f: str(f) == fileName, sourceFiles)
      if len(fileList) is 0:
        headerFile = SourceFile(fileName)
        sourceFiles.append(headerFile)
        currentFile.includes.append(headerFile)
        currentFile = headerFile
        if len(flags) > 1:
          if int(flags[1]) == 3:
            currentFile.isSystemHeader = True
      else:
        if fileList[0] not in currentFile.includes:
          currentFile.includes.append(fileList[0])
        currentFile = fileList[0]
    elif isReturn:
      fileList = filter(lambda f: str(f) == fileName, sourceFiles)
      currentFile = fileList[0]

  else:
    # source line, count if not empty
    sourceLine = line.strip()
    if len(sourceLine) is not 0:
      currentFile.nrLines += 1

currentFile = None
sourceFiles = []
rootFile = None

parser = argparse.ArgumentParser(description='Analyzes GCC preprocessor output to determine the *weight* of include statements. The weight is defined as the number of non-whitespace lines after preprocessing, that an include statement would add to the compilation unit, if it was the only statement. Run compile job with -save-temps and run this script on the resulting .ii file.')
parser.add_argument('filename', metavar='filename', help='path to preprocessed file')
parser.add_argument('-d', '--depth', type=int, default=4, help='depth of shown include tree')
parser.add_argument('-s', '--system', action='store_true', help='do not stop at system headers')
args = parser.parse_args()

for line in fileinput.input(args.filename):
  process(line)

rootFile.show(args.depth, not args.system)
