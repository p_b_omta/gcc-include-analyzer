Analyzer for GCC preprocessor output to determine the "weight" of include statements.
The weight is defined as the number of non-whitespace lines after preprocessing, that an include statement would add to the compilation unit, if it was the only statement.
This means that it is only an indication of the burden that an include statement represents, since it says nothing about the effort the preprocessor has to spend to parse the code. It is however a useful tool to get some idea of where your compile time is spent on.

Usage: Run a compile job with the command line parameter: -save-temps and run this script on the resulting .ii file.

Written in Python, tested with Python 2.7.1 and GCC 4.8.4. The script probably works with a wide range of Python and GCC versions.
